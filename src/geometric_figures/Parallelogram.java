package geometric_figures;

public class Parallelogram implements Figure {

    private double a, b, h;

    public Parallelogram(double a, double b, double h) {
        this.a = a;
        this.b = b;
        this.h = h;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    @Override
    public String getType() {
        return "Параллелограмм";
    }

    @Override
    public double getLengthOfLines() {
        return 2 * (a + b);
    }

    @Override
    public double getArea() {
        return a * h;
    }
}
