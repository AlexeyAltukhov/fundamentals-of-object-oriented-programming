package geometric_figures;

public class Point implements Figure{

    private double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String getType() {
        return "Точка";
    }

    @Override
    public double getLengthOfLines() {
        return 0;
    }

    @Override
    public double getArea() {
        return 0;
    }
}
