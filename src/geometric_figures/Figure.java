package geometric_figures;

public interface Figure {

    public String getType();
    public double getLengthOfLines();
    public double getArea();
}
