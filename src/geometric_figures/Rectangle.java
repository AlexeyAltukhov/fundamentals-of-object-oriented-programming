package geometric_figures;

public class Rectangle implements Figure {

    private double a, b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    @Override
    public String getType() {
        return "Прямоугольник";
    }

    @Override
    public double getLengthOfLines() {
        return 2 * (a + b);
    }

    @Override
    public double getArea() {
        return a * b;
    }
}
