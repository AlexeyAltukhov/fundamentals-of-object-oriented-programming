package geometric_figures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Figure> figures = new ArrayList<>(Arrays.asList(
                new Point(1, 3),
                new Line(1, 3, -5, -9),
                new Triangle(1, 1, 3, 9, -8, 0),
                new Circle(5),
                new Square(10),
                new Rectangle(4, 6),
                new Parallelogram(9, 5, 4)
        ));

        figures.forEach(value -> System.out.println("Тип: " + value.getType() + "\nДлина всех линий: " +
                value.getLengthOfLines() + "\nПлощадь: " + value.getArea() + "\n"));
    }
}
