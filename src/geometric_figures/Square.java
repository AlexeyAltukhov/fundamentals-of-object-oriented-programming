package geometric_figures;

public class Square implements Figure {

    private double a;

    public Square(double a) {
        this.a = a;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    @Override
    public String getType() {
        return "Квадрат";
    }

    @Override
    public double getLengthOfLines() {
        return a * 4;
    }

    @Override
    public double getArea() {
        return a * a;
    }
}
