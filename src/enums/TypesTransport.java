package enums;

import java.util.ArrayList;

public class TypesTransport {
    private final String name;

    private TypesTransport(String name) {
        this.name = name;
        values.add(this);
    }

    public static final ArrayList<TypesTransport> values = new ArrayList<>();

    public static final TypesTransport GROUND = new TypesTransport("ground");
    public static final TypesTransport UNDERGROUND = new TypesTransport("underground");
    public static final TypesTransport AIR = new TypesTransport("air");
    public static final TypesTransport WATER = new TypesTransport("water");

    public static TypesTransport valueOf(String name) {
        for (TypesTransport myEnum: values){
            if (myEnum.name.equalsIgnoreCase(name))
                return myEnum;
        }
        throw new IllegalArgumentException("Нет константы перечисления с именем " + name);
    }

    @Override
    public String toString() {
        return name;
    }
}

