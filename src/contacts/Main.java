package contacts;

import java.util.*;

public class Main {

    private static final Contacts contacts = new Contacts();

    public static void main(String[] args) {
        System.out.println("#1");
        addContact(null, null);

        System.out.println("#2");
        addContact("Евгений", null);

        System.out.println("#3");
        addContact("Алексей", new HashSet<>(Arrays.asList("88005553535", "89009009090")));
        addContact("Валера", new HashSet<>(Arrays.asList("88441001100", "+79009001212", "+78008008822")));
        addContact("Мама", new HashSet<>(Collections.singletonList("89990001222")));

        System.out.println("#4");
        addContact("Алексей", new HashSet<>(Collections.singletonList("89019019191")));

        System.out.println("#5");
        findPhoneNumbersByName("");

        System.out.println("#6");
        findPhoneNumbersByName("Настя");

        System.out.println("#7");
        findPhoneNumbersByName("Алексей");

        System.out.println("#8");
        findContactsByNamePart("атата");

        System.out.println("#9");
        findContactsByNamePart("ле");
    }

    public static void addContact(String name, HashSet<String> phoneNumbers) {
        try {
            contacts.add(name, phoneNumbers);
            System.out.println("Новый контакт успешно добавлен");
        } catch (ContactsException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void findPhoneNumbersByName(String name) {
        try {
            HashSet<String> phoneNumbers = contacts.findPhoneNumbersByName(name);
            if (phoneNumbers == null || phoneNumbers.isEmpty()) {
                System.out.println("Номеров для имени '" + name + "' не найдено");
            } else {
                System.out.println("Телефонные номера контакта с именем '" + name + "':");
                phoneNumbers.forEach(System.out::println);
            }
        } catch (ContactsException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void findContactsByNamePart(String partOfName) {
        try {
            Map<String, HashSet<String>> foundContacts = contacts.findContactsByNamePart(partOfName);
            if (foundContacts == null || foundContacts.isEmpty()) {
                System.out.println("Контактов по части имени '" + partOfName + "' не найдено");
            } else {
                System.out.println("Контакты, найденные по части имени '" + partOfName + "':");
                foundContacts.forEach((key, value) -> System.out.println(key + ": " + String.join(", ", value)));
            }
        } catch (ContactsException e) {
            System.out.println(e.getMessage());
        }
    }
}
