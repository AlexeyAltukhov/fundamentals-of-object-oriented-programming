package contacts;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Pattern;

public class Contacts {

    private final Map<String, HashSet<String>> contacts = new HashMap<>();

    public void add(String name, HashSet<String> phoneNumbers) throws ContactsException {
        if (name == null || name.isEmpty()) {
            throw new ContactsException("Не указано имя контакта");
        } else if (phoneNumbers == null || phoneNumbers.isEmpty()) {
            throw new ContactsException("Номер телефона для контакта не указан");
        } else if (contacts.containsKey(name)) {
            throw new ContactsException("Контакт с таким именем уже существует");
        } else {
            contacts.put(name, phoneNumbers);
        }
    }

    public HashSet<String> findPhoneNumbersByName(String name) throws ContactsException {
        if (name == null || name.isEmpty()) {
            throw new ContactsException("Не указано имя контакта, по которому будет осуществляться поиск");
        }
        return contacts.get(name);
    }

    public Map<String, HashSet<String>> findContactsByNamePart(String partOfName) throws ContactsException {
        if (partOfName == null || partOfName.isEmpty()) {
            throw new ContactsException("Не указана часть имени, по которой будет осуществляться поиск");
        }

        Map<String, HashSet<String>> result = new HashMap<>();

        for (Map.Entry<String, HashSet<String>> value : contacts.entrySet()) {
            if (Pattern.compile(partOfName.toLowerCase()).matcher(value.getKey().toLowerCase()).find())
                result.put(value.getKey(), value.getValue());
        }

        return result;
    }
}